package com.atlassian.stash.unapprove;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.stash.event.pull.PullRequestRescopedEvent;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestParticipant;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.util.Operation;
import com.google.common.collect.Iterables;

public class RescopeListener {

    private final TransactionTemplate txTemplate;
    private final SecurityService securityService;
    private final PullRequestService pullRequestService;
    private final AutoUnapproveSettings autoUnapproveSettings;

    public RescopeListener(TransactionTemplate txTemplate, SecurityService securityService,
                           PullRequestService pullRequestService, AutoUnapproveSettings autoUnapproveSettings) {
        this.txTemplate = txTemplate;
        this.securityService = securityService;
        this.pullRequestService = pullRequestService;
        this.autoUnapproveSettings = autoUnapproveSettings;
    }

    @EventListener
    public void onPullRequestRescoped(PullRequestRescopedEvent event) {
        final PullRequest pullRequest = event.getPullRequest();
        // only withdraw approval if the pull request's _source_ branch has changed, not it's target.
        if (!event.getPreviousFromHash().equals(pullRequest.getFromRef().getLatestChangeset()) &&
                autoUnapproveSettings.isEnabled(pullRequest.getToRef().getRepository())) {
            txTemplate.execute(new TransactionCallback<Void>() {
                @Override
                public Void doInTransaction() {
                    for (PullRequestParticipant participant : Iterables.concat(pullRequest.getReviewers(), pullRequest.getParticipants())) {
                        // the withdrawApproval method withdraws approval for the currently authenticated user,
                        // so use SecurityService to impersonate the reviewer we are withdrawing approval for
                        securityService.impersonating(participant.getUser(), "Unapproving pull-request on behalf of user").call(new Operation<Object, RuntimeException>() {
                            @SuppressWarnings("ConstantConditions")
                            @Override
                            public Object perform() {
                                return pullRequestService.withdrawApproval(
                                        pullRequest.getToRef().getRepository().getId(),
                                        pullRequest.getId()
                                );
                            }
                        });
                    }
                    return null;
                }
            });
        }
    }

}
